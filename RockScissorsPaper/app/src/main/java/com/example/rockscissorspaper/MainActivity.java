package com.example.rockscissorspaper;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    Button btn_rock, btn_scissors, btn_paper;
    TextView tv_score;
    ImageView iv_ComputerChoice, iv_HumanChoice;

    int HumanScore = 0;
    int ComputerScore = 0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_paper = findViewById(R.id.btn_paper);
        btn_rock = findViewById(R.id.btn_rock);
        btn_scissors = findViewById(R.id.btn_scissors);

        iv_ComputerChoice = findViewById(R.id.iv_ComputerChoice);
        iv_HumanChoice = findViewById(R.id.iv_HumanChoice);

        tv_score = findViewById(R.id.tv_score);

        btn_paper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_HumanChoice.setImageResource(R.drawable.paper);
                String message = play_turn("paper");
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                tv_score.setText("Score Human: " + Integer.toString(HumanScore) + "Computer: " + Integer.toString(ComputerScore));

            }
        });

        btn_scissors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_HumanChoice.setImageResource(R.drawable.scissor);
                String message = play_turn("scissors");
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                tv_score.setText("Score Human: " + Integer.toString(HumanScore) + "Computer: " + Integer.toString(ComputerScore));
            }
        });

        btn_rock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_HumanChoice.setImageResource(R.drawable.rock);
                String message = play_turn("rock");
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                tv_score.setText("Score Human: " + Integer.toString(HumanScore) + "Computer: " + Integer.toString(ComputerScore));
            }
        });
    }

    public String play_turn(String player_choice){
        String computer_choice= "";
        Random r = new Random();

        //choose 1 2 or 3
        int computer_choice_number = r.nextInt(3) + 1;

        if(computer_choice_number == 1){
            computer_choice = "rock";
        }
        else if(computer_choice_number ==2){
            computer_choice = "scissors";
        }
        else if(computer_choice_number == 3){
            computer_choice = "paper";
        }

        //set the computer image base on what the computer chose
        if(computer_choice == "rock"){
            iv_ComputerChoice.setImageResource(R.drawable.rock);
        }
        else if(computer_choice == "scissors"){
            iv_ComputerChoice.setImageResource(R.drawable.scissor);
        }
        else if(computer_choice == "paper"){
            iv_ComputerChoice.setImageResource(R.drawable.paper);
        }

        //compare the human choice to player choice
        if(computer_choice == player_choice){
            return "Draw. Nobody won.";
        }
        else if(player_choice == "rock" && computer_choice == "scissors"){
            HumanScore++;
            return "Rock crushes scissors. You win!";
        }
        else if(player_choice == "rock" && computer_choice == "paper"){
            ComputerScore++;
            return "Paper covers Rock. Computer wins!";
        }
        else if(player_choice == "scissors" && computer_choice == "rock"){
            ComputerScore++;
            return "Rock crushes scissors. Computer wins!";
        }
        else if(player_choice == "scissors" && computer_choice == "paper"){
            HumanScore++;
            return "Scissors cuts paper. You win!";
        }
        else if(player_choice == "paper" && computer_choice == "rock"){
            HumanScore++;
            return "Paper covers Rock. You win!";
        }
        else if(player_choice == "paper" && computer_choice == "scissors"){
            ComputerScore++;
            return "Scissors cut paper. Computer wins!";
        }
        else return"Not Sure";
    }
}
